package pig_latin;

import static org.junit.Assert.*;

import org.junit.Test;

public class Translatortest {

	@Test
	public void TestInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator= new Translator(inputPhrase);
		assertEquals("hello world",translator.getPhrase());
	}


	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator= new Translator(inputPhrase);
		assertEquals(Translator.NIL,translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithY() {
		String inputPhrase = "any";
		Translator translator= new Translator(inputPhrase);
		assertEquals("anynay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator= new Translator(inputPhrase);
		assertEquals("utilitynay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator= new Translator(inputPhrase);
		assertEquals("appleyay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator= new Translator(inputPhrase);
		assertEquals("askay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithOneConsonant() {
		String inputPhrase = "hello";
		Translator translator= new Translator(inputPhrase);
		assertEquals("ellohay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonant() {
		String inputPhrase= "known";
		Translator translator= new Translator(inputPhrase);
		assertEquals("ownknay",translator.translate());
	}
	
	@Test
	public void testTranslatingPhraseWithMoreWords() {
		String inputPhrase= "hello world";
		Translator translator= new Translator(inputPhrase);
		assertEquals("ellohay orldway",translator.translate());
	}
	
	@Test
	public void testTranslatingPhraseWithCompositeWords() {
		String inputPhrase= "well-being";
		Translator translator= new Translator(inputPhrase);
		assertEquals("ellway-eingbay",translator.translate());
	}
	
	@Test
	public void testTranlatingPhraseContainingPunctuactions() {
		String inputPhrase= "hello world!";
		Translator translator= new Translator(inputPhrase);
		assertEquals("ellohay orldway!",translator.translate());
	}
}
