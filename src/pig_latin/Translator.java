package pig_latin;

public class Translator {
	
	public static final  String NIL= "nil";
	
	private String phrase;
	private String phrase2="";
	private String phrase3="";
	private String punctuation="";

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
		
		
	}


	public String getPhrase() {
		return phrase;
		
	}


	public String translate() {
		
		if(phrase.isEmpty()) {
			return NIL;
		}
		
		
		if(phrase.contains(" ")) {
			String words[]=phrase.split(" ");
		
			
			int NumOfWords=words.length;
			Translator translator;
			
			for(int i=0;i<NumOfWords-1;i++) {
				translator= new Translator(words[i]);
				
				
				
				phrase2= phrase2 + translator.translate() + " ";
			}
			
			
			translator= new Translator(words[NumOfWords-1]);
			
			
			phrase2= phrase2 + translator.translate();
			
			return phrase2;
			
		}else {
			if(phrase.contains("-")) {
				String words[]=phrase.split("-");
				
				int NumOfWords=words.length;
				
				Translator translator;
				
				for(int i=0;i<NumOfWords-1;i++) {
					translator= new Translator(words[i]);
					phrase3= phrase3 + translator.translate() + "-";
				}
				
				translator= new Translator(words[NumOfWords-1]);
				phrase3= phrase3 + translator.translate();
				return phrase3;
			
		
				}
		
			}
		
		
		String punctuation="";
		
		//parole singole
		if(startWithVowel()) {
			
		if(phrase.endsWith("y")) {

			if(punctuations()) {
				punctuation=phrase.charAt(phrase.length()-1)+"";
			};
			
			return phrase + "nay" + punctuation;
		} else if (endWithVowel()) {
			
			if(punctuations()) {
				punctuation=phrase.charAt(phrase.length()-1)+"";
			};
			
			return phrase + "yay" + punctuation;
			
		}else if(!endWithVowel()) {
			
			if(punctuations()) {
				punctuation=phrase.charAt(phrase.length()-1)+"";
			};
			
			return phrase + "ay"+ punctuation;
		}
		
		} else {
			if(startsWithOneConsonant(1)) {
				return exchangeConsonants(1);
			}else {
					int cons=2;
					
					while(startsWithOneConsonant(cons)== false ) {
						cons++;
					};
					
					return exchangeConsonants(cons);
				  }
		}
	return NIL;
	}
	
	
	
	
	


	private boolean punctuations() {
		return phrase.endsWith(".") ||  phrase.endsWith(",")  ||  phrase.endsWith(";") ||  phrase.endsWith(":") ||  phrase.endsWith("?") ||  phrase.endsWith("!") ||  phrase.endsWith("'") ||  phrase.endsWith("(") ||  phrase.endsWith(")");
		
	}


	//scusi professore avevo butterfly chiuso per questa userstory(US4) me ne sono accorto solo dopo aver fatto la commit
	private String exchangeConsonants(int i) {  
		
		char c[]= phrase.toCharArray();
		phrase2= phrase.substring(i);
		
		if(punctuations()) {
			punctuation=phrase.charAt(phrase.length()-1)+"";
			phrase2= phrase2.substring(0,phrase2.length()-1);
		};
		
		for(int j=0;j<i;j++) {
			phrase2= phrase2 + c[j];
		}
		
		
		
		
		return phrase2 + "ay"+ punctuation;
	}


	private boolean startsWithOneConsonant(int i) {
		
		
		return phrase.startsWith("a",i) ||  phrase.startsWith("e",i)  ||  phrase.startsWith("i",i) ||  phrase.startsWith("o",i) ||  phrase.startsWith("u",i);
		
	}


	
	
	
	
	private boolean startWithVowel() {
		
			return phrase.startsWith("a") ||  phrase.startsWith("e")  ||  phrase.startsWith("i") ||  phrase.startsWith("o") ||  phrase.startsWith("u");
		}
		
	private boolean endWithVowel() {
		
		return phrase.endsWith("a") ||  phrase.endsWith("e")  ||  phrase.endsWith("i") ||  phrase.endsWith("o") ||  phrase.endsWith("u");
	}
		
	
	
	}
